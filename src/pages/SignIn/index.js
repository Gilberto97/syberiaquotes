import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import LinkButton from '../../components/LinkButton'
import Input from '../../components/Input';
import Button from '../../components/Button';
import { signIn, signOut } from '../../store/actions/index';

import './style.scss';



class SignIn extends Component {

  state = {
    controls: {
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Adres e-mail'
        },
        value: '',
        validation: {
          required: true,
          minLength: 2,
          maxLength: 40,
        },
        valid: false,
        touched: false,
      },
      password: {
        elementType: 'input',
        elementConfig: {
          type: 'password',
          placeholder: 'Hasło'
        },
        value: '',
        validation: {
          required: true,
          minLength: 6,
          maxLength: 20,
        },
        valid: false,
        touched: false,
      },
    },
    formIsValid: false,
  }

  checkValidity(value, rules) {
    let isValid = true;
    
    if (rules.required) {
      isValid = value.trim() !=='' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }
    console.log(isValid);
    return isValid;
  }

  inputChangeHandler = (event, controlName) => {
    const updatedControls = {
      ...this.state.controls,
        [controlName]:{
          ...this.state.controls[controlName],
          value:event.target.value,
          valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
          touched: true,
        }
    };

    let formIsValid = false;
    const { controls } = this.state;
    let formKeys = Object.keys(controls);
    formIsValid = !formKeys.some(item => {
      return !controls[item].valid;
    })

    this.setState({
      controls: updatedControls,
      formIsValid: formIsValid,
    })
  }

  submitHandler = (event) => {
    event.preventDefault();
    const user = {
      email: this.state.controls.email.value,
      password: this.state.controls.password.value,
    }
    this.props.signIn(user)
  }

  logoutHandler = (event) => {
    event.preventDefault();
    this.props.signOut();
  }
  
  render() {
    let { controls, formIsValid } = this.state;
    let { auth, isEmpty, displayName } = this.props;
    let formKeys = Object.keys(controls);
    console.log(this.props)
    

    return (
      <div className="auth container">
        <div className="container__links">
          <LinkButton
            faClass='fas fa-times'
            text='Strona Główna'
            link='/'/>
          <LinkButton
            faClass='fas fa-sign-in-alt'
            text='Dołącz'
            link='/signup'/>
        </div>
        {
          isEmpty ?
          <form className="auth__form" onSubmit={this.submitHandler}>
          {auth.authError ? <h2>{auth.authError}</h2> : <h1>Zaloguj</h1>}
          {
            formKeys.map((item,index)=>{
              return <Input 
                key={index}
                elementType={controls[item].elementType} 
                elementConfig={controls[item].elementConfig}
                value={controls[item].value}
                invalid={!controls[item].valid}
                shouldValidate={controls[item].validation}
                touched={controls[item].touched}
                changed={(e)=>this.inputChangeHandler(e,item)}
                />
            })
          }
          
          <Button disabled={!formIsValid} text="Zaloguj"/>

        </form> :
        <form className="auth__form" onSubmit={this.logoutHandler}>
          <h2>dzień dobry</h2>
          {<h2 className="auth__form__email">{displayName}</h2>}
          <Link to="/" exact>
            <Button text="Strona główna"/>
          </Link>

        </form> 
        }
        
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    ...state,
    authError: state.auth.authError,
    isEmpty: state.firebase.auth.isEmpty,
    email: state.firebase.auth.email,
    displayName: state.firebase.auth.displayName,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    signIn: (user) => dispatch(signIn(user)),
    signOut: () => dispatch(signOut()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
