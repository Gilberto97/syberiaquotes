import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const channel = ({
  name,
  link,
}) => {
  return (
    <li className="channel">
      <a href={link} target="_blank" rel="noopener noreferrer">
        <h1 className="channel__title"><i className="fab fa-youtube"></i> {name}</h1>
      </a>
    </li>
  )
}

channel.propTypes = {
  name: PropTypes.string.isRequired,
};

channel.defaultProps = {
  //suggestions: [],
};


export default channel;