import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import Channel from './channel';


const results = ({
  channels,
}) => {
  return (
    <ul className="results">
      {
        channels.map((item, index) => {
          console.log(item);
          return(
            <Channel
              name={item.name}
              link={item.link}
              key={index}
            />
          );
        })
      }
    </ul>
  )
}

results.propTypes = {
  channels: PropTypes.array.isRequired,
};

results.defaultProps = {
  //suggestions: [],
};


export default results;