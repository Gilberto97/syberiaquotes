import React, { Component } from 'react';
import '../../../node_modules/@fortawesome/fontawesome-free/css/all.min.css'
import { CHANNELS_STATES } from './config';
import LinkButton from '../../components/LinkButton';
import MessageBox from '../../components/MessageBox';
import axios from '../../axios-db';
import Results from './results';
import './style.scss';

class Channels extends Component {

  channels = [];

  state = {
    channelsState: CHANNELS_STATES.LOADING,
  }

  componentDidMount() {

    axios.get('https://syberiaquotes.firebaseio.com/channels.json')
      .then(response => {
        return response.data;
      })
      .then(response => {

        // this.props.history.push('/');
        
        let dbKeys=Object.keys(response);

        dbKeys.map(key => {
          return this.channels.push({
            name:key,
            link:response[key]
          })
        })

        // this.channels = channels;
        console.log(this.channels)
        this.setState({
          channelsState : CHANNELS_STATES.READY
        })
      })
      .catch(error => {
        this.setState({
          channelsState: CHANNELS_STATES.ERROR,
        })
      })
  }

  render() {
    console.log(this.channels)
    const { channelsState } = this.state;

    return (
      <React.Fragment>
      
        {
          channelsState===CHANNELS_STATES.LOADING && 
          <MessageBox
            // text="Ładowanie kanałów..."
          />
        }

        {
          channelsState===CHANNELS_STATES.ERROR && 
          <MessageBox
            text="Coś poszło nie tak..."
          />
        }
      
      
        {
          channelsState===CHANNELS_STATES.READY && 
          <div className="channels container">
            <div className="container__links">
              <LinkButton
                faClass='fas fa-times'
                text='Strona Główna'
                link='/'/>
            </div>
            <div className="channels__playlist">                 
              <iframe title="playlist_iframe" src="http://www.youtube.com/embed/?listType=user_uploads&list=syberia2official" width="100%" height="100%"></iframe>
            </div>

            {/* <h1 classname="channels__header">Lista popularnych kanałów YouTube</h1> */}

            <Results
              channels={this.channels}
            />
          </div>
        }
      </React.Fragment>
      
    )
  }
}

export default Channels
