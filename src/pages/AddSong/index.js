import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import LinkButton from '../../components/LinkButton'


import Input from '../../components/Input';
import Button from '../../components/Button';
import axios from '../../axios-db';
import { ADDSONG_STATES } from './config';
import MessageBox from '../../components/MessageBox';
import './style.scss';


class AddSong extends Component {

  state = {
    addSongState: ADDSONG_STATES.READY,
    formIsValid: false,
    songForm: {
      title: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Tytuł'
        },
        value: '',
        validation: {
          required: true,
          minLength: 2,
          maxLength: 40,
        },
        valid: false,
        touched: false,
      },
      lyrics: {
        elementType: 'textarea',
        elementConfig: {
          type: 'text',
          placeholder: 'Tekst'
        },
        value: '',
        validation: {
          required: true,
          minLength: 100,
          maxLength: 4000,
        },
        valid: false,
        touched: false,
      },
      link: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Link'
        },
        value: '',
        validation: {
          required: true,
          minLength: 20,
          maxLength: 40,
        },
        valid: false,
        touched: false,
      },
    },
    
  }

  addSongHandler = (e) => {
    const { isVerified } = this.props;

    if (isVerified) {
      e.preventDefault();
      const { songForm } = this.state;
      let formKeys = Object.keys(songForm)
      const formData = {};
      formKeys.map(item=>{
        return formData[item] = this.state.songForm[item].value;
      })
      // console.log(formData);

      this.setState({
        addSongState: ADDSONG_STATES.LOADING
      })
      
      axios.post( '/orders.json', formData )
      // .then( response => {
      //     this.props.history.push( '/' );
      // })
      .then(()=>{
        this.setState({
          addSongState: ADDSONG_STATES.SUCCESS
        });
      })
      .catch( error => {
        this.setState({
          addSongState: ADDSONG_STATES.ERROR
        });
      } );
    }
    
  }

  checkValidity(value, rules) {
    let isValid = true;
    
    if (rules.required) {
      isValid = value.trim() !=='' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    return isValid;
  }

  inputChangeHandler = (e, inputIdentifier) => {
    // console.log(inputIdentifier)
    const updatedSongForm = {...this.state.songForm};
    const updatedFormElement = {...updatedSongForm[inputIdentifier]}
    updatedFormElement.value = e.target.value;
    updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation)
    updatedFormElement.touched = true;
    updatedSongForm[inputIdentifier] = updatedFormElement;

    let formIsValid = false;
    const { songForm } = this.state;
    let formKeys = Object.keys(songForm);
    formIsValid = !formKeys.some(item => {
      return !songForm[item].valid;
    })
    // console.log();

    this.setState({
      songForm: updatedSongForm,
      formIsValid: formIsValid,
    });
  }
  
  render() {
    const { isEmpty, isVerified } = this.props;
    if(isEmpty) return <Redirect to='/signin'/> 

    let { songForm, formIsValid, addSongState } = this.state;
    let formKeys = Object.keys(songForm);
    // console.log(!formIsValid, isVerified)

    return (
      
      <React.Fragment>
        {
          addSongState===ADDSONG_STATES.LOADING && 
          <MessageBox
            text="Ładowanie..."
          />
        }

        {
          addSongState===ADDSONG_STATES.ERROR && 
          <MessageBox
            text="Coś poszło nie tak..."
          />
        }

        {
          addSongState===ADDSONG_STATES.SUCCESS && 
          <MessageBox
            text="Piosenka dodana pomyślnie i wkrótce zostanie sprawdzona"
          />
        }

        {
          addSongState===ADDSONG_STATES.READY && 
          <div className="add-song container">
           <div className="container__links">
              <LinkButton
                faClass='fas fa-times'
                text='Strona Główna'
                link='/'/>
            </div>
            <form className="add-song__form" onSubmit={this.addSongHandler}>
              {
                formKeys.map((item,index)=>{
                  return <Input 
                    key={index}
                    elementType={songForm[item].elementType} 
                    elementConfig={songForm[item].elementConfig}
                    value={songForm[item].value}
                    invalid={!songForm[item].valid}
                    shouldValidate={songForm[item].validation}
                    touched={songForm[item].touched}
                    changed={(e)=>this.inputChangeHandler(e,item)}/>
                })
              }
              <Button disabled={!formIsValid || !isVerified} text="Potwierdź"/>
              {
                !isVerified && <p className="red">Zweryfikuj email aby dodać tekst!</p>
              }

            </form>

          </div>
        }
      </React.Fragment>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    isEmpty: state.firebase.auth.isEmpty,
    isVerified: state.firebase.auth.emailVerified,
    uid: state.firebase.auth.uid
  }
}

// const mapDispatchToProps = dispatch => {
//   return {
//     addSongAfterLogin: () => dispatch(redirectAfterLogin(path)),
    
//   }
// }

export default connect(mapStateToProps)(AddSong);