import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import Suggest from './suggest';


const results = ({
  suggestions,
}) => {
  return (
    <ul className="results">
      {
        suggestions.map((item,index) => {

          let classes="suggest__";
          suggestions.length>1 ?
          classes+="multi" : 
          classes+="single";
          
          return(
            <Suggest
              id={item.id}
              title={item.title}
              text={item.lyrics}
              hint={item.help}
              link={item.link}
              classes={classes}
              index={index}
            />
          );
        })
      }
    </ul>
  )
}

results.propTypes = {
  suggestions: PropTypes.array.isRequired,
};

results.defaultProps = {
  //suggestions: [],
};


export default results;