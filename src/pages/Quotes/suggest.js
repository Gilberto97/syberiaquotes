import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './style.scss';

const suggest = ({
  id,
  title,
  text,
  hint,
  link,
  classes,
  index,
}) => {
  const dbLink = `/database/${id}`
  return (
    <Link to={dbLink}>
      <li className={"suggest "+classes}>
        <h1 className="suggest__title">{title}</h1>
        <p className="suggest__span"><span className="suggest__text">{text}</span><span className="suggest__hint">{hint}</span></p>
      </li>
    </Link>
  )
}

suggest.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  hint: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
};

suggest.defaultProps = {
  //suggestions: [],
};


export default suggest;