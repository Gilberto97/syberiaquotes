import React, { Component } from 'react';
import TextInput from '../../components/TextInput';
import LinkButton from '../../components/LinkButton';
import MessageBox from '../../components/MessageBox';
import axios from 'axios';
import { QUOTES_STATES } from './config';
import Results from './results';
import Info from './info';
import './style.scss';

class Quotes extends Component {

  clearSongs = [];

  state = {
    quotesState: QUOTES_STATES.LOADING,
    suggestions: [],
    inputValue: ""
  }

  textHint = (mainTab,valueTab) => {
    
    let result = {
      row:[],
      help:[]
    };
    
    for (let i = 0; i<mainTab.length;i++){
        
      let lastword = valueTab[valueTab.length-1]
      let mainHit =[]

      if(valueTab[0]===mainTab[i] && mainTab[i+valueTab.length-1]!==undefined && mainTab[i+valueTab.length-1].substring(0,lastword.length)===lastword){
        mainHit = mainTab.slice(i,i+valueTab.length);
        if(JSON.stringify(mainHit.slice(0,mainHit.length-1))===JSON.stringify(valueTab.slice(0,valueTab.length-1))){
        
          result.row=valueTab
          result.help.push(mainTab[i+valueTab.length-1].slice(lastword.length))
          result.help.push(mainTab[i+valueTab.length])
          result.help.push(mainTab[i+valueTab.length+1])
          break;
        }
      }
    }

    return result
  }

  clearTab = (lyrics) => {
    let clearLyrics = [];
    lyrics = lyrics.split(" ")
    for(let i=0;i<lyrics.length;i++){
      lyrics[i] = lyrics[i].toLowerCase()
      lyrics[i] = lyrics[i].replace(/[?./|&;$%@"<>()+,]/g, "")
      lyrics[i] = lyrics[i].replace(/ę/ig,'e');
      lyrics[i] = lyrics[i].replace(/ż/ig,'z');
      lyrics[i] = lyrics[i].replace(/ó/ig,'o');
      lyrics[i] = lyrics[i].replace(/ł/ig,'l');
      lyrics[i] = lyrics[i].replace(/ć/ig,'c');
      lyrics[i] = lyrics[i].replace(/ś/ig,'s');
      lyrics[i] = lyrics[i].replace(/ź/ig,'z');
      lyrics[i] = lyrics[i].replace(/ń/ig,'n');
      lyrics[i] = lyrics[i].replace(/ą/ig,'a');
    }

    clearLyrics = lyrics;
    clearLyrics = clearLyrics.filter((word) => word!=="")
    
    return clearLyrics;
  }

  onTextChanged = (e) => {

    let { clearSongs } = this;

    var value = this.clearTab(e.target.value);

    this.setState(() =>({
      inputValue:value
    }));

    let suggestions = []

    for (let i = 0; i<clearSongs.length;i++){
      let song_lyric = clearSongs[i].lyrics
      let song_link = clearSongs[i].link
      let result = this.textHint(song_lyric,value)
      let row = result.row.join(" ");
      let help = result.help.join(" ");

      // console.log(clearSongs[i].id)
      if(row.length>0){
        suggestions.push({
          id: clearSongs[i].id,
          title: clearSongs[i].title,
          lyrics : row,
          help : help,
          link : "https://www.youtube.com/embed/"+song_link
        })
      }
    }

    suggestions.sort((a,b) => (a.hits < b.hits) ? 1 : ((b.hits < a.hits) ? -1 : 0)); 
    suggestions = suggestions.slice(0, 10);

    console.log(suggestions)
    this.setState(()=> ({suggestions}))
    
  }


  componentDidMount() {

    axios.get('http://localhost:3000/songs')
      .then(response => {
        return response.data;
      })
      .then(response => {
        console.log(response)
        // this.props.history.push('/');
        
        let dbKeys=Object.keys(response)
       
        for(let i=0; i<dbKeys.length;i++){
          this.clearSongs.push({
            id:response[dbKeys[i]]._id,
            title:response[dbKeys[i]].title,
            lyrics:this.clearTab(response[dbKeys[i]].lyrics),
            link:response[dbKeys[i]].link,
          })
        }

        this.setState({
          quotesState : QUOTES_STATES.READY
        })
      })
      .catch(error => {
        this.setState({
            quotesState: QUOTES_STATES.ERROR,
        })
      })
  }

  render() {
    const { quotesState } = this.state;
    return (
      <React.Fragment>
        {
          quotesState===QUOTES_STATES.LOADING && 
          <MessageBox
            // text="Ładowanie utworów..."
          />
        }

        {
          quotesState===QUOTES_STATES.ERROR && 
          <MessageBox
            text="Coś poszło nie tak..."
          />
        }
      
        {
          quotesState===QUOTES_STATES.READY && 
            <div className="quotes container">
              <div className="container__links">
                <LinkButton
                  faClass='fas fa-times'
                  text='Strona Główna'
                  link='/'/>
                <LinkButton
                  faClass='fab fa-itunes-note'
                  text='Dodaj'
                  link='/addsong'/>
              </div>
              
                
              {/* {
                this.state.suggestions.length<1 && this.state.inputValue.length<1 &&
                <h1 className="quotes__input-info">Co Ci chodzi po głowie?</h1>
              } */}

              <div className="quotes__input-back">
                <TextInput
                  text="Wpisz tekst piosenki!"
                  onTextChange={this.onTextChanged}/>
              </div>

              {
                this.state.inputValue.length===1 ?
                <Info
                  text="Wpisz conajmniej 2 wyrazy!"
                /> :
                <Results
                  suggestions={this.state.suggestions}
                /> 
              }

            </div>
          }
      </React.Fragment>
    )
  }
}

export default Quotes;