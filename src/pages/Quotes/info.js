import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const info = ({
  text,
}) => {
  return (
    <div className="info">
        <h1>{text}</h1>
    </div>
  )
}

info.propTypes = {
  text: PropTypes.string,
};

info.defaultProps = {
  text:"",
};


export default info;