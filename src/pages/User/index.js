import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import LinkButton from '../../components/LinkButton'
import { Redirect } from 'react-router-dom';
import Input from '../../components/Input';
import Button from '../../components/Button';
import { signOut, sendEmailVerification } from '../../store/actions/index';

import './style.scss';



class User extends Component {

  state = {

  }

  getDataString = (timeStamp) => {
    // return Date.toISOString(timeStamp).slice(0,10)
    // var event = new Date(timeStamp).getYear);
    return new String(new Date().toISOString()).slice(0,10)
  }


  
  render() {
    let { auth, isEmpty, signOut, displayName, email, isVerified, isVerifyEmailSent, verifyEmail, createdAt, lastLoginAt } = this.props;
    
    if(isEmpty) return <Redirect to='/signin'/> 

    // this.props.firebase.auth.displayName
    return (
      <div className="user container">
        <div className="container__links">
          <LinkButton
            faClass='fas fa-times'
            text='Strona Główna'
            link='/'/>
        </div>
        {
          auth.uid ?
          <div>
            <h1>Coś poszło nie tak!</h1>
          </div> :
          <div className="user__div">
            <p>{displayName}</p>
            <p>{email}</p>
            <p>Dołączył {this.getDataString(createdAt)}</p>
            <p>Ostatnio {this.getDataString(lastLoginAt)}</p>
            {/* <div className="user__buttons"> */}
              <Button text="Wyloguj" action={signOut}/>
              {
                isVerified 
                ? null
                : !isVerifyEmailSent
                  ? <Button text="Zweryfikuj" action={verifyEmail}/>
                  : <div class="verify-panel">
                    <p>e-mail wysłany</p><i class="fas fa-sync fa-spin"/>
                  </div>
              }
              {
                
              }
            {/* </div> */}
          </div>
        }
        
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    ...state,
    authError: state.auth.authError,
    isEmpty: state.firebase.auth.isEmpty,
    email: state.firebase.auth.email,
    displayName: state.firebase.auth.displayName,
    isVerified: state.firebase.auth.emailVerified,
    isVerifyEmailSent: state.auth.isVerifyEmailSent,
    createdAt: state.firebase.auth.createdAt,
    lastLoginAt: state.firebase.auth.lastLoginAt,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    signOut: () => dispatch(signOut()),
    verifyEmail: () => dispatch(sendEmailVerification())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(User);
