import React, { Component } from 'react';
import LinkButton from '../../components/LinkButton';
import MessageBox from '../../components/MessageBox';
import FieldList from '../../components/FieldList';
import axios from 'axios';
import { SD_STATES } from './config';
import './style.scss';

class SongDetails extends Component {


  state = {
    dbState: SD_STATES.LOADING,
    song:{}
  }

  componentDidMount() {
    const songID = this.props.match.params.id
    axios.get(`http://localhost:3000/songs/${songID}`)
      .then(response => {
        return response.data;
      })
      .then(response => {
        this.setState({
          song: response
        })
      })
      .then(response => {
        
        // song = response
        console.log("song",this.state.song);

        this.setState({
          dbState : SD_STATES.READY
        })
      })
      .catch(error => {
        this.setState({
            dbState: SD_STATES.ERROR,
        })
      })
      
  }

  render() {
    const { dbState } = this.state;
    return (
      <React.Fragment>

        {
          dbState===SD_STATES.LOADING && 
          <MessageBox
            text="Loading Songs..."
          />
        }

        {
          dbState===SD_STATES.ERROR && 
          <MessageBox
            text="Coś poszło nie tak..."
          />
        }
      
        {
          dbState===SD_STATES.READY && 
          <div className="database container">
              <div className="container__links">
                <LinkButton
                  faClass='fas fa-arrow-left'
                  text='Wróć'
                  link='/database'/>
                <LinkButton
                  faClass='fas fa-times'
                  text='Strona Główna'
                  link='/'/>
              </div>          
              <FieldList
                dict={this.state.song}
              />
              
          </div>
        } 
      </React.Fragment>
    )
  }
}

export default SongDetails;