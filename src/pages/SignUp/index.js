import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';

import LinkButton from '../../components/LinkButton'
import Input from '../../components/Input';
import Button from '../../components/Button';
import { signUp } from '../../store/actions/index';

import './style.scss';



class SignIn extends Component {

  state = {
    controls: {
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Adres e-mail'
        },
        value: '',
        validation: {
          required: true,
          minLength: 2,
          maxLength: 40,
        },
        valid: false,
        touched: false,
      },
      password1: {
        elementType: 'input',
        elementConfig: {
          type: 'password',
          placeholder: 'Hasło'
        },
        value: '',
        validation: {
          required: true,
          minLength: 6,
          maxLength: 20,
        },
        valid: false,
        touched: false,
      },
      password2: {
        elementType: 'input',
        elementConfig: {
          type: 'password',
          placeholder: 'Powtórz Hasło'
        },
        value: '',
        validation: {
          required: true,
          minLength: 6,
          maxLength: 20,
          equals: true,
        },
        valid: false,
        touched: false,
      },
      name: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Pseudonim'
        },
        value: '',
        validation: {
          required: true,
          minLength: 3,
          maxLength: 15,
        },
        valid: false,
        touched: false,
      },
    },
    formIsValid: false,
  }

  checkValidity(value, rules) {
    let isValid = true;
    
    if (rules.required) {
      isValid = value.trim() !=='' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    if (rules.equals) {
      isValid = value === this.state.controls.password1.value && isValid;
    }
    console.log(isValid);
    return isValid;
  }

  inputChangeHandler = (event, controlName) => {
    const updatedControls = {
      ...this.state.controls,
        [controlName]:{
          ...this.state.controls[controlName],
          value:event.target.value,
          valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
          touched: true,
        }
    };

    let formIsValid = false;
    const { controls } = this.state;
    let formKeys = Object.keys(controls);
    formIsValid = !formKeys.some(item => {
      return !controls[item].valid;
    })
    console.log(formIsValid);

    this.setState({
      controls: updatedControls,
      formIsValid: formIsValid,
    })
  }

  submitHandler = (event) => {
    event.preventDefault();
    const newUser = {
      email: this.state.controls.email.value,
      password: this.state.controls.password1.value,
      name: this.state.controls.name.value,
    }
    this.props.signUp(newUser)
  }

  logoutHandler = (event) => {
    event.preventDefault();
    this.props.signOut();
  }
  
  render() {
    let { controls, formIsValid } = this.state;
    let { auth, isEmpty } = this.props;
    
    let formKeys = Object.keys(controls);
    console.log(this.props)
    

    return (
      <div className="auth container">
        <div className="container__links">
          <LinkButton
            faClass='fas fa-times'
            text='Strona Główna'
            link='/'/>
          <LinkButton
            faClass='fas fa-door-closed'
            text='Wejdź'
            link='/signin'/>
        </div>
        {
          isEmpty ?
          <form className="auth__form" onSubmit={this.submitHandler}>
            {auth.authError ? <h2>{auth.authError}</h2> : <h1>Zarejestruj się!</h1>}
            {
              formKeys.map((item,index)=>{
                return <Input 
                  key={index}
                  elementType={controls[item].elementType} 
                  elementConfig={controls[item].elementConfig}
                  value={controls[item].value}
                  invalid={!controls[item].valid}
                  shouldValidate={controls[item].validation}
                  touched={controls[item].touched}
                  changed={(e)=>this.inputChangeHandler(e,item)}
                  />
              })
            }
            <Button disabled={!formIsValid} text="Zarejestruj"/>

          </form> :
          <form className="auth__form" onSubmit={()=>{}}>
            <h2 className="auth__form__h1">Rejestracja przebiegła pomyślnie!</h2>
            <Link to="/" exact>
              <Button text="Strona główna"/>
            </Link>
          </form> 
        }
        
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    ...state,
    authError: state.auth.authError,
    isEmpty: state.firebase.auth.isEmpty,
    email: state.firebase.auth.email,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    signUp: (newUser) => dispatch(signUp(newUser)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
