export const DB_STATES = {
    LOADING: 0,
    READY: 1,
    ERROR: 2,
};

export const TABLE_CONFIG = [
    {
        id: 'id',
        text: 'ID',
        field: 'id',
        type: 'text'
    },
    {
        id: 'title',
        text: 'Tytuł',
        field: 'title',
        type: 'text'
    },
    {
        id: 'words',
        text: 'Słowa',
        field: 'words',
        type: 'text'
    },
    {
        id: 'details_button',
        text: 'Tekst',
        type: 'button',
        link: '/database/',
        buttonText: 'Pokaż'
    }
];
