import React from "react";
import './style.scss'
import PropTypes from 'prop-types';

const dbInfo = ({
    songsNum,
    wordsNum,
}) => {
  return (
    <div className="db-info" >
      <div className="db-info__tile">
        <div>
          <h1>Utworów</h1>
          <p>{songsNum}</p>
        </div>
        <div>
          <h1>Słów</h1>
          <p>{wordsNum}</p>
        </div>
      </div>

      <div className="db-info__tile">
        <h1>Dziekujemy!</h1>
      </div>
    
    </div>
  );
};

dbInfo.propTypes = {
  songsNum: PropTypes.string,
  wordsNum: PropTypes.string,
};

dbInfo.defaultProps = {
  songsNum: '-',
  wordsNum: '-',
};

export default dbInfo;
