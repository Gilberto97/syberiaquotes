import React, { Component } from 'react';
// import TextInput from '../../components/TextInput';
import LinkButton from '../../components/LinkButton';
import MessageBox from '../../components/MessageBox';
import Table from '../../components/Table';
import DBInfo from './dbInfo';
import axios from 'axios';
import { DB_STATES, TABLE_CONFIG } from './config';
import './style.scss';

class DataBase extends Component {

  songs = [];
  songsNum = 0;
  wordsNum = 0;

  state = {
    dbState: DB_STATES.LOADING,
    //inputValue: ""
  }

  componentDidMount() {

    axios.get('http://localhost:3000/songs')
      .then(response => {
        return response.data;
      })
      .then(response => {
                
        // let dbKeys=Object.keys(response)

        response.map((item,index) => {
          return this.songs.push({
            id: item._id,
            title: item.title,
            words: item.lyrics.split(" ").length,
          })
        })

        this.songsNum = this.songs.length;

        this.songs.forEach(item => {
          this.wordsNum+=item.words;
        })

        this.setState({
          dbState : DB_STATES.READY
        })
      })
      .catch(error => {
        this.setState({
            dbState: DB_STATES.ERROR,
        })
      })
      
  }

  render() {
    const { dbState } = this.state;
    return (
      <React.Fragment>

        {
          dbState===DB_STATES.LOADING && 
          <MessageBox
            text="Loading Songs..."
          />
        }

        {
          dbState===DB_STATES.ERROR && 
          <MessageBox
            text="Coś poszło nie tak..."
          />
        }
      
        {
          dbState===DB_STATES.READY && 
          <div className="database container">
              <div className="container__links">
                <LinkButton
                  faClass='fas fa-times'
                  text='Strona Główna'
                  link='/'/>
                <LinkButton
                  faClass='fab fa-itunes-note'
                  text='Dodaj'
                  link='/addsong'/>
              </div>
              <DBInfo
                songsNum={this.songsNum}
                wordsNum={this.wordsNum}
              />
          
              <Table
                items={this.songs}
                config={TABLE_CONFIG}/> 
          
          </div>
        }
      </React.Fragment>
    )
  }
}

export default DataBase;