import React from "react";
import './style.scss'



const article = () => {
  return (
    <div className='article'>
      <h3>Projekt jest open source, zachęcam programistów do współpracy.</h3>
      <a href=""><i className="article__icon-big fas fa-code-branch"></i></a>
      <p>Aplikacja nie jest i nie będzie komercyjna, nikt nie czerpie i nie będzie czerpał z niej zysków.</p>
      {/* <div className="article_icons">
        <i className="article__icon fa-react"></i>
        <i className="article__icon fab fa-js"></i>
        <i className="article__icon fa-css3-alt"></i>
        <i className="article__icon fab fa-html5"></i>
      </div> */}
      <p>Wszystkie teksty są własnością Liryki Składu Ostródzkiego.</p>
    </div>
  );
};

// header.propTypes = {
//   text: PropTypes.string,
//   onChange: PropTypes.func,
// };

// header.defaultProps = {
//   text: 'WRITE SMT',
//   onChange: ()=>{},
// };

export default article;