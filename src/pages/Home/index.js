import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import Header from './header';
import Tile from './tile';
import Article from './article';
import './style.scss';


class Home extends Component {

  
  render() {
    let { displayName } = this.props;

    return (
      <div className="home">
        <Header/>
          <div className="container">
            <Link exact to="/quotes">
              <Tile
                header="Znajdź"
                // text="Wpisz frazę tekstu i znajdź swój utwór"
                faIconClass="fas fa-search"
                classes="tile1"/>
            </Link>
            {this.props.isEmpty && <Link exact to="/signup">
              <Tile
                header="Dołącz"
                faIconClass="fas fa-sign-in-alt"
                // text="Dołącz do Nas i pomagaj katalogować teksty"
                classes="tile7"/>
            </Link>}
            
            {this.props.isEmpty && <Link exact to="/signin">
              <Tile
                  header="Wejdź"
                  faIconClass="fas fa-door-closed"
                  // text="Zaloguj się na swoje konto"
                  classes="tile6"/>
            </Link>}

            {!this.props.isEmpty && <Link exact to="/user">
              <Tile
                  header={displayName}
                  faIconClass="fas fa-door-open"
                  // text="Zobacz swój profil"
                  classes="tile8"/>
            </Link>}

            <Link exact to="/channels">
              <Tile
                header="Odkryj"
                faIconClass="fab fa-itunes-note"
                // text="Zobacz kanały z muzyką na platformie YouTube"
                classes="tile2"/>
            </Link>
            <Link exact to="/database">
              <Tile
                header="Baza"
                faIconClass="fas fa-database"
                // text="Zobacz utwory w bazie danych"
                classes="tile3"/>
            </Link>

            <Link exact to="/addsong">
              <Tile
                header="Dodaj"
                faIconClass="far fa-plus-square"
                // text="Dodaj tekst do bazy danych"
                classes="tile4"/>
            </Link>

            <Article/>
            
          </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    ...state,
    isEmpty: state.firebase.auth.isEmpty,
    displayName: state.firebase.auth.displayName,
  }
}

export default connect(mapStateToProps)(Home);