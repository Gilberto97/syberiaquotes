import React from "react";
import './style.scss'
// import PropTypes from 'prop-types';


const tile = ({
    header,
    text,
    faIconClass,
    classes
}) => {
  return (
    <div className={"tile "+classes}>
      <div>
        {header && <h2>{header}</h2>}
        {text && <p>{text}</p>}
        {faIconClass && <i className={faIconClass}></i>}
      </div>
    </div>
  );
};

// textInput.propTypes = {
//     text: PropTypes.string,
//     onChange: PropTypes.func,
//   };
  
//   textInput.defaultProps = {
//     text: 'WRITE SMT',
//     onChange: ()=>{},
//   };

export default tile;
