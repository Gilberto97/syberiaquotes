import React from "react";
import './style.scss'
// import PropTypes from 'prop-types';

import logo from '../../images/logo.gif'

const header = () => {
  return (
    <div className='header'>
      <img  
        src={logo} 
        alt="logo"/>
      <h1>SyberiaQuotes</h1>
    </div>
  );
};

// header.propTypes = {
//   text: PropTypes.string,
//   onChange: PropTypes.func,
// };

// header.defaultProps = {
//   text: 'WRITE SMT',
//   onChange: ()=>{},
// };

export default header;
