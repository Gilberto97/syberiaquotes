import * as actionTypes from '../actions/actionTypes';

const initialState = {
    isAuthenticated: false,
    token: '',
    authError: null,
}

const authReducer = ( state = initialState, action ) => {
  switch ( action.type ) {
    case actionTypes.SIGNIN_ERROR:
      return {
          ...state,
          authError: 'Błąd Logowania!'
      }
    case actionTypes.SIGNIN_SUCCESS:
      console.log("Auth success")
      return {
          ...state,
          authError: null,
      }
    case actionTypes.SIGNOUT_SUCCESS:
      return {}
    case actionTypes.SIGNUP_ERROR:
      return {
          ...state,
          authError: 'Błąd Rejestracji!'
      }
    case actionTypes.SIGNUP_SUCCESS:
      return {
          ...state,
          authError: null,
      }

    
    default:
      return state;
  }
};

export default authReducer;