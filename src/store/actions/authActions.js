import * as actionTypes from './actionTypes';
import axios from '../../axios-db';
import { firebase_users } from '../../firebaseConfig';

export const signIn = (user) => {
    return (dispatch, getState, { getFirebase }) => {
        const firebase = getFirebase();
        firebase.auth().signInWithEmailAndPassword(user.email, user.password)
        .then(()=> {
            dispatch({type: actionTypes.SIGNIN_SUCCESS});
        })
        .catch((err)=> {
            dispatch({type: actionTypes.SIGNIN_ERROR});
        })
    }
}

export const signOut = () => {
    return (dispatch, getState, { getFirebase }) => {
        const firebase = getFirebase();

        firebase.auth().signOut()
        .then(()=> {
            dispatch({ type: actionTypes.SIGNOUT_SUCCESS })
        })
        .catch((err)=> {
            dispatch({type: actionTypes.SIGNOUT_ERROR});
        })
    }
}

export const signUp = (newUser) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();

    firebase.auth().createUserWithEmailAndPassword(
      newUser.email,
      newUser.password,
    )
    .then((resp)=> {
      if(resp.user) {
        resp.user.updateProfile({
          displayName: newUser.name,
        })
      }
    })
    // .then((resp)=> {
      
    //   firebase_users.child(resp.user.uid).set({
    //     name: newUser.name,
    //     songs: 0,
    //   })
    // })
    
    .then(()=> {
        dispatch({ type: actionTypes.SIGNUP_SUCCESS })
    })
    .catch((err)=> {
        dispatch({type: actionTypes.SIGNUP_ERROR, err});
    })
  }
}


export const sendEmailVerification = () => {
  return (dispatch, getState, { getFirebase }) => {
      const firebase = getFirebase();
      firebase.auth().currentUser.sendEmailVerification()
      .then(()=> {
          dispatch({ type: actionTypes.EMAIL_VERIFICATION_SEND_SUCCESS })
      })
      .catch((err)=> {
          dispatch({type: actionTypes.EMAIL_VERIFICATION_SEND_ERROR});
      })
  }
}
