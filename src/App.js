import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from './pages/Home';
import Quotes from './pages/Quotes';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import User from './pages/User';
import Channels from './pages/Channels';
import DataBase from './pages/DataBase';
import SongDetails from './pages/SongDetails';
import AddSong from './pages/AddSong';

import "./App.css";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route 
            path="/" 
            component={Home} 
            exact />

          <Route 
            path="/quotes" 
            component={Quotes} 
            exact />

          <Route 
            path="/signin" 
            component={SignIn} 
            exact />

          <Route 
            path="/user" 
            component={User} 
            exact />


          <Route 
            path="/signup" 
            component={SignUp} 
            exact />
          
          <Route 
            path="/channels" 
            component={Channels} 
            exact />
          
          <Route 
            path="/addsong" 
            component={AddSong} 
            exact />

          <Route 
            path="/database" 
            component={DataBase} 
            exact />
          
          <Route
            path="/database/:id"
            component={SongDetails}
            exact />

          {/* <Route 
            component={Page404} 
            path="/404" 
            exact /> */}
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
