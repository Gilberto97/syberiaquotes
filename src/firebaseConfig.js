import firebase from 'firebase'
import 'firebase/auth'

var config = {
    apiKey: "AIzaSyD7XRdXA1ZENm5KHowcoOM4TXq8z9L596g",
    authDomain: "syberiaquotes.firebaseapp.com",
    databaseURL: "https://syberiaquotes.firebaseio.com",
    projectId: "syberiaquotes",
    storageBucket: "syberiaquotes.appspot.com",
    messagingSenderId: "953226102720"
  };
  firebase.initializeApp(config);

  export const firebase_users = firebase.database().ref("users");

  export default firebase;

