import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const textInput = ({
  text,
  onTextChange,
}) => {
  return (
    <input className="text-input" type="text" placeholder={text} onChange={onTextChange}/>
  )
}

textInput.propTypes = {
  text: PropTypes.string,
  onTextChange: PropTypes.func,
};

textInput.defaultProps = {
  text: 'WRITE SMT',
  onTextChange: ()=>{},
};


export default textInput;