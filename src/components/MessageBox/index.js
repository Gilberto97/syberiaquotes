import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import imgURL from '../../images/info.gif';

const messageBox = ({
  text
}) => {
  return (
    <div className="message-box">
      <img src={imgURL} alt="message box"/>
      <h1>{text}</h1>
    </div>
  )
}

messageBox.propTypes = {
  text: PropTypes.string.isRequired,
};

messageBox.defaultProps = {
  text: "",
};


export default messageBox;