import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const input = (props) => {
  let { label, elementType, elementConfig, value, invalid, shouldValidate, touched } = props;
  let inputElement = null;
  let validationError = null;
  let classes = [];

  if(invalid && shouldValidate && touched) {
    classes.push("invalid");
    // validationError = <p className="validation-error">{shouldValidate.minLength}-{shouldValidate.maxLength} znaków</p>;
  }
  else if (touched) {
    classes.push("valid");
  }

  switch (elementType) {
    case ('input'):
      inputElement = <input {...elementConfig} value={value} onChange={props.changed}/>;
      break;

    case ('textarea'):
      inputElement = <textarea {...elementConfig} value={value} onChange={props.changed}/>;
      break;

    default:
      inputElement = <input {...elementConfig} value={value} onChange={props.changed}/>

  }

  return (
    <div className={"input "+classes.join(' ')}>
        <label>{label}</label>
        {inputElement}
        {validationError}
    </div>
  )
}

// input.propTypes = {
//   text: PropTypes.string,
//   onTextChange: PropTypes.func,
// };

// input.defaultProps = {
//   text: 'WRITE SMT',
//   onTextChange: ()=>{},
// };


export default input;