import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';


const fieldList = ({
  dict
}) => {
  const keys = Object.keys(dict)
  return (
    <div className="field-list">
      {
        keys.map((key) => {
          switch(key){
            case 'link':
              return (
                  <React.Fragment>
                    <div className="field-list__element">
                      <p>{key}</p>
                      <h4>{`https://www.youtube.com/embed/${dict[key]}`}</h4>
                    </div>
                    <iframe
                        title={"suggest-" + dict[key] + "-iframe"}
                        width="300"
                        height="200"
                        src={`https://www.youtube.com/embed/${dict[key]}`}
                        frameBorder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen>
                    </iframe>
                  </React.Fragment>)
            default:
              return (
                  <div className="field-list__element">
                    <p>{key}</p>
                    <h4>{dict[key]}</h4>
                  </div>
              )
          }
        })
      }
    </div>
  )
}

fieldList.propTypes = {
  dict: PropTypes.object.isRequired,
};

fieldList.defaultProps = {
  dict: {},
};


export default fieldList;