import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import './style.scss';

const linkButton = ({
  text,
  link,
  faClass
}) => {
  return (
    <Link to={link} exact>
      <div className="link-button">
        <i class={faClass}></i>
      </div>
    </Link>
  )
}

linkButton.propTypes = {
  text: PropTypes.string,
  link: PropTypes.string,
  faClass: PropTypes.string,
};

linkButton.defaultProps = {
  text: 'CLICK',
  link: '/',
  faClass: 'fas fa-times',
};


export default linkButton